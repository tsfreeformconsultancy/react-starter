import React from 'react';
import { shallow } from 'enzyme';

// Components
import App from './index';

function setup() {
  const wrapper = shallow(<App />);
  return { wrapper };
}

describe('App Test Suite', () => {
  it('Should have text "React Starter"', () => {
    const { wrapper } = setup();
    expect(wrapper.find('p').text()).toBe('React Starter');
  });
});
